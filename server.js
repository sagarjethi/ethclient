

const express = require("express");

const bodyParser = require("body-parser");
let cors = require("cors");
const web3 = require("web3");
import loggerClient from "./loggerClient";
import blockchainClient from "./blockchainClient";
import * as constant from "./helper/constant";
import * as contractHelper from "./helper/contractHelper";


global.__base = __dirname;

const app = express();
const server = require("http").createServer(app);

app.web3Client = null;
app.loggerClient = null;

// Middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.loggerClient = loggerClient;

let startWeb=async () =>{
		//init web3
		const provider = new web3.providers.HttpProvider(constant.configServer.web3Config.url);
		const web3Client = new web3(provider);
		if (await web3Client.eth.net.isListening()) {
			app.web3Client = web3Client;
			app.loggerClient("WEB3", "Initialized web3");
		} else {
			app.loggerClient("WEB3", "Unable to initialized web3");
			return null;
		}

		app.web3Helper = blockchainClient(web3Client);

		// init contract
		const contractInstance = await app.web3Helper.initializeContract(constant.CONTRACT_ADDRESS, constant.BINARY_ABI);
		contractHelper.addContract("VLD", contractInstance);


		process.on("unhandledRejection", (reason, promise) => {
			console.error("Uncaught error in", promise, reason);
		});
  };
startWeb();

server.listen(constant.configServer.port);
app.loggerClient("SERVER", "Running on port " + constant.configServer.port);
